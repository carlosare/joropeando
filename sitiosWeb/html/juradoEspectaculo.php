<?php

	require_once('../../modelos/calificaciones/claseCalificaciones.php');

	session_start();

	if ($_SESSION['modalidad'] == 'Espectaculo') {
		
		$jurado = $_SESSION['nombre']." ".$_SESSION['apellidos'];
		$idJurado = $_SESSION['id'];
		$calificaciones = new Calificaciones();
		$calificaciones->set_modalida("Espectaculo");
		$calificaciones->set_idJurado($idJurado);
		$calificaciones->get_idValidar();
		$idValidacion = $calificaciones->get_idValidacion();
		if($_GET) {
			if(array_key_exists('grupo', $_GET)) {
				$grupo = $_GET['grupo'];
				$calificaciones->set_nombreGrupo($grupo);
				$calificaciones->get_grupo();
			}
		}else{
			if ($idValidacion == 2 ) {
				$calificaciones->get_gruposCalificar();
			}
			else{
				require_once('../../modelos/calificaciones/claseCalificacionFinal.php');
				$calificaciones->get_calificacionFinal();
			}
		}							
?>

<!DOCTYPE html>
<html lang="en">
	<head>
	    <title>Joropeando 2016</title>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link rel="stylesheet" href="/joropeando/sitiosWeb/css/bootstrap.css">
	    <script src="/joropeando/sitiosWeb/js/bootstrap.js"></script>
		<script src="/joropeando/sitiosWeb/js/jquery.js"></script>
		<script type="text/javascript" src="/joropeando/sitiosWeb/js/calificaciones.js"></script>
	</head>
	<body>

		<nav class="navbar navbar-inverse">
		  	<div class="container-fluid">
		    	<div class="navbar-header">
		      		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
		        		<span class="icon-bar"></span>
		        		<span class="icon-bar"></span>
		        		<span class="icon-bar"></span> 
		      		</button>
		      		<a class="navbar-brand" href="#">WebSiteName</a>
		    	</div>
		    	<div class="collapse navbar-collapse" id="myNavbar">
		      		<ul class="nav navbar-nav">
		        		<li class="active"><a href="#">Home</a></li>
		        		<li><a href="#">Page 1</a></li>
		        		<li><a href="#">Page 2</a></li> 
		        		<li><a href="#">Page 3</a></li> 
		      		</ul>
		      		<ul class="nav navbar-nav navbar-right">
		        		<li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
		        		<li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
		      		</ul>
		    	</div>
		  	</div>
		</nav>

		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-4">
					<div>
						<h2>Grupos</h2>
						<div class="table-responsive">
						<table class="table">
						    <thead>
						        <tr>
							        <th>#</th>
							        <th>Nombre</th>
							        <th>Categoria</th>
						        </tr>
						    </thead>
						    <tbody>
						        <?php
									$calificaciones->get_gruposModalidades();
								?>
						    </tbody>
						  </table>
						  </div>
					</div>
				</div>
				<div class="col-sm-3">
					<div>
						<div>
							<h5>Nombre del grupo: <span><?php echo $calificaciones->get_nombreGrupo(); ?></span></h5>
							<h5>Categoria: <span><?php echo $calificaciones->get_categoria(); ?></span></h5>
							<h5>Academia: <span><?php echo $calificaciones->get_nombreAcademia(); ?></span></h5>
							<h5>Descripcion coreografica: <span><?php echo $calificaciones->get_descripcion(); ?></span></h5>
						</div>
						<h2>Calificaciòn</h2>
						<form class="form-horizontal" method="post" action="../../modelos/calificaciones/calificaciones.php">
							<div class="form-group">
						    	<div class="col-sm-6">
						    		<?php
						    			echo "<input type=\"hidden\" name=\"nombre_grupo\" value=\"".$calificaciones->get_nombreGrupo()."\">"; 
						    		?>
						    	</div>
						    	<div class="col-sm-6">
						      		<input type="hidden" name="modalidad" value="Espectaculo">
						    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label class="col-sm-6">Ritmo:</label>
						    	<div class="col-sm-6">
						      		<input type="number" class="form-control" id="ritmo" onKeyUp="set_ritmo()" name="ritmo" min="1" max="100" placeholder="20%" required>
						    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label class="col-sm-6">Dificulta:</label>
						    	<div class="col-sm-6">
						      		<input type="number" class="form-control" id="dificulta" onKeyUp="set_dificulta()" name="dificulta" min="1" max="100" placeholder="15%" required>
						    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label class="col-sm-6">Zapateo:</label>
						    	<div class="col-sm-6">
						      		<input type="number" class="form-control" id="zapateo" onKeyUp="set_zapateo()" name="zapateo" min="1" max="100" placeholder="20%" required>
						    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label class="col-sm-6">Creatividad:</label>
						    	<div class="col-sm-6">
						      		<input type="number" class="form-control" id="creatividad" onKeyUp="set_creatividad()" name="creatividad" min="1" max="100" placeholder="20%" required>
						    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label class="col-sm-6">Desplazamiento:</label>
						    	<div class="col-sm-6">
						      		<input type="number" class="form-control" id="desplazamiento" onKeyUp="set_desplazamiento()" name="dezplazamiento" min="1" max="100" placeholder="15%" required>
						    	</div>
						  	</div>
						  	<div class="form-group">
						    	<label class="col-sm-6">Vestuario:</label>
						    	<div class="col-sm-6">
						      		<input type="number" class="form-control" id="vestuario" onKeyUp="set_vestuario()" name="vestuario" min="1" max="100" placeholder="10%" required>
						    	</div>
						  	</div>
					
						  	<div class="form-group"> 
						    	<div class="col-sm-offset-2 col-sm-10">
						      	<button type="submit" class="btn btn btn-success">Calificar</button>
						    </div>
						  </div>
						</form>
					</div>
				</div>
				<div class="col-sm-3" id="resultado">
					<div id="total"></div>
				</div>
			</div>
		</div>
	</body>
</html>
<?php
	}else{
		header('Location:/joropeando/sitiosWeb/html/login.html');
	}
?>