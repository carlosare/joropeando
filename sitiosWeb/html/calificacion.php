<?php require_once('../../modelos/clasificacion/claseClasificacion.php');
	$clasificacion = new Clasificacion();
 ?>
<!DOCTYPE html>
<html lang="ES">
<head>
	<title>calificacion</title>
	<meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link rel="stylesheet" href="/joropeando/sitiosWeb/css/bootstrap.css">
	    <link rel="stylesheet" href="/joropeando/sitiosWeb/css/mycss.css">
</head>
<body>
	<div class="row">
	<h2>MODALIDADES ES DE LOS GRUPOS</h2>
		<div class="col-sm-6">
		<h3>Tradicional Niños</h3>
		<div class="table-responsive">
					<table class="table">
					    <thead>
					        <tr>
						        <th>#</th>
						        <th>Nombre</th>	
						        <th>Calificacion</th>				       
					        </tr>
					    </thead>
					    <tbody>
					        <?php $clasificacion->get_calificacionTradicionalNinos() ?>
					    </tbody>
					  </table>
					  </div>
			
		</div>
		<div class="col-sm-6">
			<h3>Tradicional Jovenes</h3>
			<div class="table-responsive">
					<table class="table">
					    <thead>
					        <tr>
						        <th>#</th>
						        <th>Nombre</th>	
						        <th>Calificacion</th>				        
					        </tr>
					    </thead>
					    <tbody>
					       <?php $clasificacion->get_calificacionTradicionalJovenes(); ?>
					    </tbody>
					  </table>
					  </div>
			
		</div>
		<div class="col-sm-6">
			<h3>Tradicional Adultos</h3>
			<div class="table-responsive">
					<table class="table">
					    <thead>
					        <tr>
						        <th>#</th>
						        <th>Nombre</th>
						        <th>Calificacion</th>
					        </tr>
					    </thead>
					    <tbody>
					        <?php $clasificacion->get_calificacionTradicionalAdultos(); ?>
					    </tbody>
					  </table>
					  </div>
			
		</div>
		<div class="col-sm-6">
			<h3>Espectaculo Niños</h3>
			<div class="table-responsive">
					<table class="table">
					    <thead>
					        <tr>
						        <th>#</th>
						        <th>Nombre</th>
						        <th>Calificacion</th>
					        </tr>
					    </thead>
					    <tbody>
					        <?php $clasificacion->get_calificacionEspectaculoNinos(); ?>
					    </tbody>
					  </table>
					  </div>
			
		</div>
		<div class="col-sm-6">
			<h3>Espectaculo Jovenes</h3>
			<div class="table-responsive">
					<table class="table">
					    <thead>
					        <tr>
						        <th>#</th>
						        <th>Nombre</th>
						        <th>Calificacion</th>
					        </tr>
					    </thead>
					    <tbody>
					        <?php $clasificacion->get_calificacionEspectaculoJovenes(); ?>
					    </tbody>
					  </table>
					  </div>
			
		</div>
		<div class="col-sm-6">
			<h3>mapa de ruta XD</h3>
		</div>
		
	</div>
</body>
</html>