<?php

	require_once('../../modelos/puntuaciones/clasePuntuaciones.php');
	$puntos = new Puntuaciones();
	
  ?>
  
<!DOCTYPE html>
<html lang="ES">
	<head>
		<title>categorias</title>
		 <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link rel="stylesheet" href="/joropeando/sitiosWeb/css/bootstrap.css">
	    <link rel="stylesheet" href="/joropeando/sitiosWeb/css/mycss.css">
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	    <script src="/joropeando/sitiosWeb/js/clasificacion.js"></script>
  	    <script type="text/javascript">

		function tiempoReal()
		{
			var tabla = $.ajax({
				url:'tablaConsulta.php',
				dataType:'text',
				async:false
			}).responseText;

			document.getElementById("miTabla").innerHTML = tabla;
		}
		setInterval(tiempoReal, 1000);
		</script>

	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<h2>Tradicional niños</h2>
					<div class="table-responsive">
						<table class="table">
						    <thead>
						        <tr>
							        <th>#</th>
							        <th>Nombre</th>
							        <th>Puntuacion</th>
						        </tr>
						    </thead>
						    <tbody>
						        <?php
									$puntos->get_tradicionalNiños();
								?>
						    </tbody>
						 </table>
					</div>
				</div>
				
				<div class="col-sm-4">
					<h2>Tradicional jovenes</h2>
					<div class="table-responsive">
						<table class="table">
						    <thead>
						        <tr>
							        <th>#</th>
							        <th>Nombre</th>
							        <th>Puntuacion</th>
						        </tr>
						    </thead>
						    <tbody>
						        <?php
									$puntos->get_tradicionalJovenes();
								?>
						    </tbody>
						</table>
					</div>
				</div>

				<div class="col-sm-4">
					<h2>Tradicional adultos</h2>
					<div class="table-responsive">
						<table class="table">
						    <thead>
						        <tr>
							        <th>#</th>
							        <th>Nombre</th>
							        <th>Puntuacion</th>
						        </tr>
						    </thead>
						    <tbody>
						        <?php
									$puntos->get_tradicionalAdultos();
								?>
						    </tbody>
						  </table>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<h2>Espectaculo niños</h2>
					<div class="table-responsive">
						<table class="table">
						    <thead>
						        <tr>
							        <th>#</th>
							        <th>Nombre</th>
							        <th>Puntuacion</th>
						        </tr>
						    </thead>
						    <tbody id="miTabla">
						        
						    </tbody>
						  </table>
					</div>
				</div>
				<div class="col-sm-6">
					<h2>Espectaculo niños</h2>
					<div class="table-responsive">
						<table class="table">
						    <thead>
						        <tr>
							        <th>#</th>
							        <th>Nombre</th>
							        <th>Puntuacion</th>
						        </tr>
						    </thead>
						    <tbody>
						        <?php
									$puntos->get_espectaculoJovenes();
								?>
						    </tbody>
						  </table>
					</div>
				</div>			
			</div>

			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4">
					<div>
					  	<span id="result"></span>
					</div>
					<div>
						<input type="button" class="btn btn-danger btn-block" id="enviar" value="Cerrar ronda">
					</div>
				</div>
			</div>
		</div>
	</body>
</html>