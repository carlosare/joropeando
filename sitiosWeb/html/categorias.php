<?php

	require_once('../../modelos/categoria/claseCategorias.php');
	$categorias = new Categorias();
	
  ?>
  
<!DOCTYPE html>
<html lang="ES">
	<head>
		<title>categorias</title>
		 <meta charset="utf-8">
		    <meta name="viewport" content="width=device-width, initial-scale=1">
		    <link rel="stylesheet" href="/joropeando/sitiosWeb/css/bootstrap.css">
		    <link rel="stylesheet" href="/joropeando/sitiosWeb/css/mycss.css">
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<h2>niños</h2>
					<div class="table-responsive">
						<table class="table">
						    <thead>
						        <tr>
							        <th>#</th>
							        <th>Nombre</th>
							        <th>Categoria</th>
						        </tr>
						    </thead>
						    <tbody>
						        <?php
									$categorias->get_categoriaNinos();
								?>
						    </tbody>
						 </table>
					</div>
				</div>
				
				<div class="col-sm-4">
					<h2>jovenes</h2>
					<div class="table-responsive">
						<table class="table">
						    <thead>
						        <tr>
							        <th>#</th>
							        <th>Nombre</th>
							        <th>Categoria</th>
						        </tr>
						    </thead>
						    <tbody>
						        <?php
									$categorias->get_categoriaJovenes();
								?>
						    </tbody>
						</table>
					</div>
				</div>

				<div class="col-sm-4">
					<h2>adultos</h2>
					<div class="table-responsive">
						<table class="table">
						    <thead>
						        <tr>
							        <th>#</th>
							        <th>Nombre</th>
							        <th>Categoria</th>
						        </tr>
						    </thead>
						    <tbody>
						        <?php
									$categorias->get_categoriaAdultos();
								?>
						    </tbody>
						  </table>
					</div>
				</div>
			</div>

			<div class="row">
				<h2>MODALIDADES DE LOS GRUPOS</h2>
				<div class="col-sm-6">
					<h3>Tradicional Niños</h3>
					<div class="table-responsive">
						<table class="table">
						    <thead>
						        <tr>
							        <th>#</th>
							        <th>Nombre</th>					       
						        </tr>
						    </thead>
						    <tbody>
						        <?php $categorias->get_modalidadTradicionalNinos() ?>
						    </tbody>
						</table>
					</div>
				</div>

				<div class="col-sm-6">
					<h3>Tradicional Jovenes</h3>
					<div class="table-responsive">
						<table class="table">
						    <thead>
						        <tr>
							        <th>#</th>
							        <th>Nombre</th>					        
						        </tr>
						    </thead>
						    <tbody>
						       <?php $categorias->get_modalidadTradicionalJovenes(); ?>
						    </tbody>
						</table>
					</div>				
				</div>

				<div class="col-sm-6">
					<h3>Tradicional Adultos</h3>
					<div class="table-responsive">
						<table class="table">
						    <thead>
						        <tr>
							        <th>#</th>
							        <th>Nombre</th>
						        </tr>
						    </thead>
						    <tbody>
						        <?php $categorias->get_modalidadTradicionalAdultos(); ?>
						    </tbody>
						</table>
					</div>
				</div>

				<div class="col-sm-6">
					<h3>Espectaculo Niños</h3>
					<div class="table-responsive">
						<table class="table">
						    <thead>
						        <tr>
							        <th>#</th>
							        <th>Nombre</th>
						        </tr>
						    </thead>
						    <tbody>
						        <?php $categorias->get_modalidadEspectaculoNinos(); ?>
						    </tbody>
						</table>
					</div>
				</div>

				<div class="col-sm-6">
					<h3>Espectaculo Jovenes</h3>
					<div class="table-responsive">
						<table class="table">
						    <thead>
						        <tr>
							        <th>#</th>
							        <th>Nombre</th>
						        </tr>
						    </thead>
						    <tbody>
						        <?php $categorias->get_modalidadEspectaculoJovenes(); ?>
						    </tbody>
						</table>
					</div>				
				</div>

				<div class="col-sm-6">
					<h3>mapa de ruta XD</h3>
				</div>			
			</div>
		</div>
	</body>
</html>