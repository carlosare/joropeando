$(document).ready(function() {
  $('#login').click(function(){
    var user = $('#user').val();
    var pass = $('#pass').val();
    if($.trim(user).length > 0 && $.trim(pass).length > 0){
      $.ajax({
        url:"../../modelos/login/login.php",
        method:"POST",
        data:{user:user, pass:pass},
        cache:"false",
        beforeSend:function(){
          $('#login').val("Conectando...");
        },
        success:function(data){
          $('#login').val("Entrar");
          if (data == "admin"){
            $(location).attr('href','formularioRegistro.php');
            exit();
          } 
          if (data == "Espectaculo"){
            $(location).attr('href','juradoEspectaculo.php');
            exit();
          }
          if (data == "Tradicional"){
            $(location).attr('href','JuradoTradicional.php');
            exit();
          }
          if (data == "juradoCerrado") {
            $(location).attr('href','JuradoCerrado.html');
            exit();
          }
          if (data == "adminCerrado") {
            $(location).attr('href', 'categorias.php');
            exit();
          }
          else{  
            $("#result").html("<div class='alert alert-dismissible alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>¡Error!</strong> Usuario o contraseña invalido.</div>");
          }
        }
      });
    }else{
      $("#result").html("<div class='alert alert-dismissible alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>¡Error!</strong> Llene los campos para seguir.</div>");
    };
  });

  $('#registro').click(function(){
    var nombreGrupo = $('#nombreGrupo').val();
    var modalidad = $('#modalidad').val();
    var categoria = $('#categoria').val();
    var nombreAcademia = $('#nombreAcademia').val();
    var nombreDirector = $('#nombreDirector').val();
    var apellidosDirector = $('#apellidosDirector').val();
    var descripcion = $('#descripcion').val();
    var ubicacion = $('#ubicacion').val();
    if ($.trim(nombreGrupo).length > 0 && $.trim(nombreAcademia).length > 0 && $.trim(nombreDirector).length > 0 && $.trim(apellidosDirector).length > 0 && $.trim(descripcion).length >0 && $.trim(ubicacion).length > 0){
      $.ajax({
        url:"../../modelos/registro/formularioRegistro.php",
        method:"POST",
        data:{nombreGrupo:nombreGrupo, modalidad:modalidad, categoria:categoria, nombreAcademia:nombreAcademia, nombreDirector:nombreDirector, apellidosDirector:apellidosDirector, descripcion:descripcion, ubicacion:ubicacion},
        cache: "false",
        beforeSend:function(){
          $('#registro').val("conectando...");
        },
        success:function(data){
          $('#registro').val("Registrar");
          if (data == "error-1") {
            $("#result").html("<div class='alert alert-dismissible alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>¡Error!</strong> El nombre del grupo ya fue registrado.</div>");
            exit();
          }
          if (data == "error-2") {
            $("#result").html("<div class='alert alert-dismissible alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>¡Error!</strong> El puesto ubicacion ya esta ocupada.</div>");
            exit();
          }
          if (data == "exito") {
            $(location).attr('href','registroexito.html');
            exit();
          }
          else{
            $("#result").html("Comunicate con el desarrollador");
          }
        }
      });
    }else{
      $("#result").html("<div class='alert alert-dismissible alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>¡Error!</strong> Llene todos los campos para segir.</div>");
    };
  });

  $('#cerrarRegistro').click(function(){
    $.ajax({
      url:"../../modelos/registro/cerarrRegsitro.php",
      cache:"false",
      beforeSend:function(){
        $('#cerrarRegistro').val("Conectando...");
      },
      success:function(data){
        $('#cerrarRegistro').val("Cerrando..");
        if (data == "notCloset") {
          $("#cerrar").html("<div class='alert alert-dismissible alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>¡Error!</strong> No hay mas de 10 grupos registrados.</div>");  
        }else{
          $(location).attr('href','categorias.php');
        }
      }
    });
  });
});