$(document).ready(function(){
	$('#enviar').click(function(){
		$.ajax({
			url:"../../modelos/puntuaciones/cerrarCalificacion.php",
			cache:"falce",
			beforeSend:function(){
				$('#enviar').val("Conectando...");
			},
			success:function(data){
				$('#enviar').val("Enviando..");
				if (data == "exito") {
					$(location).attr('href', 'clasificacion.php');
				}
				if (data == "error") {
					 $("#result").html("<div class='alert alert-dismissible alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>¡Error!</strong> Aun no se an calificado todos los grupos.</div>");
				}
			}
		});
	});
});
	