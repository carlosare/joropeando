<?php
	
		require_once('claseFormularioRegistro.php');

		$nombreGrupo = $_POST["nombreGrupo"];
		$modalidad = $_POST["modalidad"];
		$categoria = $_POST["categoria"];
		$nombreAcademia = $_POST["nombreAcademia"];
		$nombreDirector = $_POST["nombreDirector"];
		$apellidosDirectro = $_POST["apellidosDirector"];
		$descripcion = $_POST["descripcion"];
		$ubicacion = $_POST["ubicacion"];

		$formularioRegistro = new FormularioRegistro();

		$formularioRegistro->set_nombreGrupo($nombreGrupo);
		$formularioRegistro->set_modalida($modalidad);
		$formularioRegistro->set_categoria($categoria);
		$formularioRegistro->set_nombreAcademia($nombreAcademia);
		$formularioRegistro->set_nombreDirector($nombreDirector);
		$formularioRegistro->set_apellidosDirector($apellidosDirectro);
		$formularioRegistro->set_descripcion($descripcion);
		$formularioRegistro->set_ubicacion($ubicacion);
		$formularioRegistro->grupo();
		
?>