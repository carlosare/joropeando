<?php

      require_once('../../nucleo/claseDatos.php');

	class Categorias extends Datos{


		public function get_categoriaNinos(){
			$this->sql="SELECT `nombre_grupo`,`modalida_grupo` FROM `grupos` WHERE `categoria_grupo` like 'niños'";
			$CategoriaNiños = $this->get_query();
			for ($i=0; $i < $this->_colum; $i++){
				$j = $i+1;
				echo "<tr>";
				echo "<td>".$j."</td>";
      		      for ($x=0; $x <= 1; $x++) { 
      			   echo "<td>".$CategoriaNiños[$i][$x]."</td>";
      		      }
      		      echo "</tr>";
      	      }
            }

            public function get_categoriaJovenes(){
			$this->sql="SELECT `nombre_grupo`,`modalida_grupo` FROM `grupos` WHERE `categoria_grupo` like 'jovenes'";
			$CategoriaJovenes = $this->get_query();
			for ($i=0; $i < $this->_colum; $i++){
				$j = $i+1;
				echo "<tr>";
				echo "<td>".$j."</td>";
            		for ($x=0; $x <= 1; $x++) { 
            			echo "<td>".$CategoriaJovenes[$i][$x]."</td>";
            		}
            		echo "</tr>";
            	}
            }

            public function get_categoriaAdultos(){
			$this->sql="SELECT `nombre_grupo`,`modalida_grupo` FROM `grupos` WHERE `categoria_grupo` like 'adultos'";
			$CategoriaAdultos = $this->get_query();
			for ($i=0; $i < $this->_colum; $i++){
				$j = $i+1;
				echo "<tr>";
				echo "<td>".$j."</td>";
            		for ($x=0; $x <= 1; $x++) { 
            			echo "<td>".$CategoriaAdultos[$i][$x]."</td>";
            		}
            		echo "</tr>";
            	}
            }

            public function get_modalidadTradicionalNinos(){
            	$this->sql="SELECT `nombre_grupo` FROM `grupos` WHERE `modalida_grupo` = 'tradicional' && `categoria_grupo` = 'niños'";
            	$modalidaTradicionalNinos = $this->get_query();
                  $j = 0;
            	if ($this->_matriz) {
            		for ($i=0; $i < $this->_colum; $i++){
					$j = $i+1;
					echo "<tr>";
					echo "<td>".$j."</td>";
	            		for ($x=0; $x < 1; $x++) { 
	            			echo "<td>".$modalidaTradicionalNinos[$i][$x]."</td>";
            		    }
            		echo "</tr>";
            	    }
            	    echo "totalidad de grupos: ".$j;
            	}
            }

            public function get_modalidadTradicionalAdultos(){
            	$this->sql="SELECT `nombre_grupo` FROM `grupos` WHERE `modalida_grupo` = 'tradicional' && `categoria_grupo` = 'adultos'";
            	$modalidadTradicionalAdultos = $this->get_query();
                  $j = 0;
            	for ($i=0; $i < $this->_colum; $i++){
				$j = $i+1;
				echo "<tr>";
				echo "<td>".$j."</td>";
            		for ($x=0; $x < 1; $x++) { 
            			echo "<td>".$modalidadTradicionalAdultos[$i][$x]."</td>";
            		}
            		echo "</tr>";
                  }
                  echo "totalidad de grupos: ".$j;
            }
             public function get_modalidadTradicionalJovenes(){
            	$this->sql="SELECT `nombre_grupo` FROM `grupos` WHERE `modalida_grupo` = 'tradicional' && `categoria_grupo` = 'jovenes'";
            	$modalidadTradicionalJovenes = $this->get_query();
                  $j = 0;
            	for ($i=0; $i < $this->_colum; $i++){
				$j = $i+1;
				echo "<tr>";
				echo "<td>".$j."</td>";
            		for ($x=0; $x < 1; $x++) { 
            			echo "<td>".$modalidadTradicionalJovenes[$i][$x]."</td>";
            		}
            		echo "</tr>";
                  }
                  echo "totalidad de grupos: ".$j;
            }

             public function get_modalidadEspectaculoNinos(){
            	$this->sql="SELECT `nombre_grupo` FROM `grupos` WHERE `modalida_grupo` = 'espectaculo' && `categoria_grupo` = 'niños'";
            	$modalidadEspectaculoNinos = $this->get_query();
                  $j = 0;
            	for ($i=0; $i < $this->_colum; $i++){
				$j = $i+1;
				echo "<tr>";
				echo "<td>".$j."</td>";
            		for ($x=0; $x < 1; $x++) { 
            			echo "<td>".$modalidadEspectaculoNinos[$i][$x]."</td>";
            		}
            		echo "</tr>";
                  }
                  echo "totalidad de grupos: ".$j;
            }
            
             public function get_modalidadEspectaculoJovenes(){
            	$this->sql="SELECT `nombre_grupo` FROM `grupos` WHERE `modalida_grupo` = 'espectaculo' && `categoria_grupo` = 'jovenes'";
            	$modalidadEspectaculoJovenes = $this->get_query();
                  $j = 0;
            	for ($i=0; $i < $this->_colum; $i++){
				$j = $i+1;
				echo "<tr>";
				echo "<td>".$j."</td>";
            		for ($x=0; $x < 1; $x++) { 
            			echo "<td>".$modalidadEspectaculoJovenes[$i][$x]."</td>";
            		}
            		echo "</tr>";
                  }
                  echo "totalidad de grupos: ".$j;
            }
      }

  ?>