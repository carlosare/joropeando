<?php

      require_once('../../nucleo/claseDatos.php');

	class Puntuaciones extends Datos{


		public function get_tradicionalNiños(){
			$this->sql="SELECT `nombre_grupo`,FLOOR(`CALI`) FROM (SELECT `grupos`.`nombre_grupo`,AVG(`calficacion_total`)AS `CALI` FROM `calificaiones`,`grupos` WHERE `grupos`.`modalida_grupo`='Tradicional' && `grupos`.`categoria_grupo`= 'niños' && `grupos`.`id_grupos`= `calificaiones`.`grupos_id_grupos` GROUP BY `grupos_id_grupos`) AS `TB1` ORDER BY `CALI`";
                        $calificacionTradicionalNinos = $this->get_query();
                  for ($i=0; $i < $this->_colum; $i++){
                              $j = $i+1;
                              echo "<tr>";
                              echo "<td>".$j."</td>";
                        for ($x=0; $x <= 1; $x++) { 
                              echo "<td>".$calificacionTradicionalNinos[$i][$x]."</td>";
                        }
                        echo "</tr>";
                       
                  }
            }

            public function get_tradicionalJovenes(){
			$this->sql="SELECT `nombre_grupo`,FLOOR(`CALI`) FROM (SELECT `grupos`.`nombre_grupo`,AVG(`calficacion_total`)AS `CALI` FROM `calificaiones`,`grupos` WHERE `grupos`.`modalida_grupo`='Tradicional' && `grupos`.`categoria_grupo`= 'jovenes' && `grupos`.`id_grupos`= `calificaiones`.`grupos_id_grupos` GROUP BY `grupos_id_grupos`) AS `TB1` ORDER BY `CALI`";
                        $calificacionTradicionalJovenes = $this->get_query();
                  for ($i=0; $i < $this->_colum; $i++){
                              $j = $i+1;
                              echo "<tr>";
                              echo "<td>".$j."</td>";
                        for ($x=0; $x <= 1; $x++) { 
                              echo "<td>".$calificacionTradicionalJovenes[$i][$x]."</td>";
                        }
                        echo "</tr>";
                       
                  }
            }

            public function get_tradicionalAdultos(){
			$this->sql="SELECT `nombre_grupo`,FLOOR(`CALI`) FROM (SELECT `grupos`.`nombre_grupo`,AVG(`calficacion_total`)AS `CALI` FROM `calificaiones`,`grupos` WHERE `grupos`.`modalida_grupo`='Tradicional' && `grupos`.`categoria_grupo`= 'adultos' && `grupos`.`id_grupos`= `calificaiones`.`grupos_id_grupos` GROUP BY `grupos_id_grupos`) AS `TB1` ORDER BY `CALI`";
                        $calificacionTradicionalAdultos = $this->get_query();
                  for ($i=0; $i < $this->_colum; $i++){
                              $j = $i+1;
                              echo "<tr>";
                              echo "<td>".$j."</td>";
                        for ($x=0; $x <= 1; $x++) { 
                              echo "<td>".$calificacionTradicionalAdultos[$i][$x]."</td>";
                        }
                        echo "</tr>";
                       
                  }
            }

            public function get_espectaculoNiños(){
            	$this->sql="SELECT `nombre_grupo`,FLOOR(`CALI`) FROM (SELECT `grupos`.`nombre_grupo`,AVG(`calficacion_total`)AS `CALI` FROM `calificaiones`,`grupos` WHERE `grupos`.`modalida_grupo`='espectaculo' && `grupos`.`categoria_grupo`= 'niños' && `grupos`.`id_grupos`= `calificaiones`.`grupos_id_grupos` GROUP BY `grupos_id_grupos`) AS `TB1` ORDER BY `CALI`";
                        $calificacionEspectaculoNinos = $this->get_query();
                  for ($i=0; $i < $this->_colum; $i++){
                              $j = $i+1;
                              echo "<tr>";
                              echo "<td>".$j."</td>";
                        for ($x=0; $x <= 1; $x++) { 
                              echo "<td>".$calificacionEspectaculoNinos[$i][$x]."</td>";
                        }
                        echo "</tr>";
                       
                  }
            }

            public function get_espectaculoJovenes(){
                  $this->sql="SELECT `nombre_grupo`,FLOOR(`CALI`) FROM (SELECT `grupos`.`nombre_grupo`,AVG(`calficacion_total`)AS `CALI` FROM `calificaiones`,`grupos` WHERE `grupos`.`modalida_grupo`='espectaculo' && `grupos`.`categoria_grupo`= 'jovenes' && `grupos`.`id_grupos`= `calificaiones`.`grupos_id_grupos` GROUP BY `grupos_id_grupos`) AS `TB1` ORDER BY `CALI`";
                  $calificacionEspectaculoJovenes = $this->get_query();
                  for ($i=0; $i < $this->_colum; $i++){
                        $j = $i+1;
                        echo "<tr>";
                        echo "<td>".$j."</td>";
                        for ($x=0; $x <= 1; $x++) { 
                              echo "<td>".$calificacionEspectaculoJovenes[$i][$x]."</td>";
                        }
                        echo "</tr>";
                  }
            }
      }

  ?>