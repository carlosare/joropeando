<?php
	
	require_once('claseCalificaciones.php');

	session_start();

	$id_jurado = $_SESSION['id'];
	$modalidad = $_POST["modalidad"];
	$grupo = $_POST["nombre_grupo"];
	$ritmo = $_POST["ritmo"] * 0.2;
	$dificulta = $_POST["dificulta"] * 0.15;
	$zapateo = $_POST["zapateo"] * 0.2;
	$creatividad = $_POST["creatividad"] * 0.2;
	$dezplazamiento = $_POST["dezplazamiento"] * 0.15;
	$vestuario= $_POST["vestuario"] * 0.1;
	$puntaje = $ritmo + $dificulta + $zapateo + $creatividad + $dezplazamiento + $vestuario;
	$calificaciones = new Calificaciones();

	$calificaciones->set_jurado($id_jurado);
	$calificaciones->set_modalida($modalidad);
	$calificaciones->set_nombreGrupo($grupo);
	$calificaciones->set_ritmo($ritmo);
	$calificaciones->set_dificulta($dificulta);
	$calificaciones->set_zapateo($zapateo);
	$calificaciones->set_creatividad($creatividad);
	$calificaciones->set_dezplazamiento($dezplazamiento);
	$calificaciones->set_vestuario($vestuario);
	$calificaciones->set_puntaje($puntaje);
	$calificaciones->get_idValidar();
	$calificaciones->id_grupo();
	$calificaciones->set_calificaciones();
	header("Location: /joropeando/sitiosWeb/html/calificacionExito.php?modalida=".$modalidad);
