-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-11-2016 a las 22:03:15
-- Versión del servidor: 5.7.9
-- Versión de PHP: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `joropeando`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `idadmin` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) DEFAULT NULL,
  `pass` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idadmin`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`idadmin`, `usuario`, `pass`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calificaiones`
--

DROP TABLE IF EXISTS `calificaiones`;
CREATE TABLE IF NOT EXISTS `calificaiones` (
  `id_calificaiones` int(11) NOT NULL AUTO_INCREMENT,
  `ritmo` int(11) DEFAULT NULL,
  `dificuta` int(11) DEFAULT NULL,
  `zapateo` int(11) DEFAULT NULL,
  `creatividad` int(11) DEFAULT NULL,
  `desplazamiento` int(11) DEFAULT NULL,
  `vestuario` int(11) DEFAULT NULL,
  `calficacion_total` int(11) DEFAULT NULL,
  `jurados_id_jurados` int(11) NOT NULL,
  `grupos_id_grupos` int(11) NOT NULL,
  PRIMARY KEY (`id_calificaiones`),
  KEY `fk_calificaiones_jurados_idx` (`jurados_id_jurados`),
  KEY `fk_calificaiones_grupos1_idx` (`grupos_id_grupos`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `calificaiones`
--

INSERT INTO `calificaiones` (`id_calificaiones`, `ritmo`, `dificuta`, `zapateo`, `creatividad`, `desplazamiento`, `vestuario`, `calficacion_total`, `jurados_id_jurados`, `grupos_id_grupos`) VALUES
(8, 20, 15, 20, 5, 5, 4, 69, 1, 3),
(9, 4, 8, 5, 12, 11, 9, 49, 1, 5),
(10, 20, 15, 20, 20, 15, 10, 100, 3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

DROP TABLE IF EXISTS `grupos`;
CREATE TABLE IF NOT EXISTS `grupos` (
  `id_grupos` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_grupo` varchar(45) DEFAULT NULL,
  `modalida_grupo` varchar(45) DEFAULT NULL,
  `categoria_grupo` varchar(45) DEFAULT NULL,
  `nombre_academia` varchar(45) DEFAULT NULL,
  `nombres_director` varchar(45) DEFAULT NULL,
  `apellidos_director` varchar(45) DEFAULT NULL,
  `descripcion_coreografia` varchar(45) DEFAULT NULL,
  `ubicaion_grupo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_grupos`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`id_grupos`, `nombre_grupo`, `modalida_grupo`, `categoria_grupo`, `nombre_academia`, `nombres_director`, `apellidos_director`, `descripcion_coreografia`, `ubicaion_grupo`) VALUES
(3, 'Corocoras', 'Espectaculo', 'NiÃ±os', 'wdqwd', 'ffewfr', 'wefewf', 'fwefwef', 1),
(5, 'Amadeus', 'Espectaculo', 'Jovenes', 'weffewfewf', 'fewfewf', 'wefewf', 'wefwef', 3),
(6, 'La piscina', 'Tradicional', 'Adultos', 'wefwef', 'wefwef', 'wefwef', 'wefwefwe', 4),
(7, 'las fufas', 'Espectaculo', 'Jovenes', 'fesfef', 'fewfwe', 'ewfew', 'fewfew', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jurados`
--

DROP TABLE IF EXISTS `jurados`;
CREATE TABLE IF NOT EXISTS `jurados` (
  `id_jurados` int(11) NOT NULL AUTO_INCREMENT,
  `nombres_jurado` varchar(45) DEFAULT NULL,
  `apellidos_jurados` varchar(45) DEFAULT NULL,
  `modalidad_calificar` varchar(45) DEFAULT NULL,
  `usuario_jurado` varchar(45) DEFAULT NULL,
  `pass_jurado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_jurados`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `jurados`
--

INSERT INTO `jurados` (`id_jurados`, `nombres_jurado`, `apellidos_jurados`, `modalidad_calificar`, `usuario_jurado`, `pass_jurado`) VALUES
(1, 'Ricardo', 'Garcia', 'Espectaculo', 'admin', '123'),
(2, 'Carlos', 'Arevalo', 'Tradicional', 'admin', '789'),
(3, 'Vaso', 'De leche', 'Espectaculo', 'admin', '456');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `validacion`
--

DROP TABLE IF EXISTS `validacion`;
CREATE TABLE IF NOT EXISTS `validacion` (
  `id_validacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_validacion` varchar(45) DEFAULT NULL,
  `estado_validacion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_validacion`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `validacion`
--

INSERT INTO `validacion` (`id_validacion`, `nombre_validacion`, `estado_validacion`) VALUES
(1, 'Registro', 'cerrado'),
(2, 'Calificacion', 'abierto');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `calificaiones`
--
ALTER TABLE `calificaiones`
  ADD CONSTRAINT `fk_calificaiones_grupos1` FOREIGN KEY (`grupos_id_grupos`) REFERENCES `grupos` (`id_grupos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_calificaiones_jurados` FOREIGN KEY (`jurados_id_jurados`) REFERENCES `jurados` (`id_jurados`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
