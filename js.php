<!DOCTYPE html>
<html>
<head>
	<title>formulario jurado</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script> function puntuacion(){
  p1 = parseInt(document.getElementById("ritmo").value);
  p2 = parseInt(document.getElementById("dyd").value);
  p3 = parseInt(document.getElementById("zye").value);
  p4 = parseInt(document.getElementById("cyec").value);
  p5 = parseInt(document.getElementById("dpg").value);
  p6 = parseInt(document.getElementById("vestuario").value);
  r  =  ((p1*0.20)+(p2*0.15)+(p3*0.20)+(p4*0.20)+(p5*0.15)+(p6*0.10));
  document.getElementById("pf").value = r;
}</script>
</head>
<body>

<div class="container-fluid">
	<div class="row">
	<div class="col-sm-4">
		<h1>puntuacion total</h1>
		<input type="number"  id="pf" value="0">

	</div>
	<div class="col-sm-4">
  		<h2>formulario de calificacion</h2>
  		<form id="puntuacion">
    		<div class="form-group">
      			<label for="ritmo">Ritmo:</label>
      			<input type="number" class="form-control" id="ritmo" placeholder="20%" onChange="puntuacion();">
    		</div>
   	 		<div class="form-group">
      			<label for="dyd">Destreza Y Dinamica:</label>
      			<input type="number" class="form-control" id="dyd" placeholder="15%" onChange="puntuacion();">
    		</div>
    		<div class="form-group">
      			<label for="zye">Zapateo Y Escubillao:</label>
      			<input type="number" class="form-control" id="zye" placeholder="20%" onChange="puntuacion();">
    		</div>
    		<div class="form-group">
      			<label for="cyec">Creatividad Y Expresion Corporal:</label>
      			<input type="number" class="form-control" id="cyec" placeholder="20%" onChange="puntuacion();">
    		</div>
    		<div class="form-group">
      			<label for="dpg">Desplazamiento Permannete del Grupo:</label>
      			<input type="number" class="form-control" id="dpg" placeholder="15%" onChange="puntuacion();">
    		</div>
    		<div class="form-group">
      			<label for="Vestuario">Vestuario:</label>
      			<input type="number" class="form-control" id="vestuario" placeholder="10%" onChange="puntuacion();">
    		</div>
    			<button type="submit" class="btn btn-default">Envira</button>
  		</form>
	</div>
	</div>
</div>

</body>
</html>