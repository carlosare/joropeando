<?php

    abstract class mySql{
        private static $db_host = 'localhost';
        private static $db_user = 'root';
        private static $db_pass = '';
        private static $db_name = 'joropeando';
        private $conexion;
        protected $sql;
        protected $_query;
        protected $_matriz;
        protected $_colum;
        protected $_mensaje;

        private function abrir_conexion() {
            $this->conexion = new mysqli(self::$db_host, self::$db_user, self::$db_pass, self::$db_name);
        }

        private function cerrar_conexion() {
            $this->conexion->close();
        }

        protected function set_query(){
            $this->abrir_conexion();
            if ($this->conexion) {
                $this->_query = $this->conexion->query($this->sql);
            }else{
                echo "Error en la conexion a la Db -> ".$this->conexion->connect_error();
            }
            $this->cerrar_conexion();
        }

        protected function get_query() {
            $matriz = array();
            $this->abrir_conexion();
            if ($this->conexion) {
                $result = $this->conexion->query($this->sql);
                if ($result) {
                    $this->_colum = 0;
                    while($this->row = $result->fetch_array(MYSQLI_NUM)){
                        $fila = count($this->row);
                        for ($i=0; $i < $fila; $i++) {
                            $matriz[$this->_colum][$i] = $this->row[$i];
                        }
                        $this->_colum++;
                    }
                    return $matriz;
                }else{
                    echo "Error en la consulta -> ";
                }   
            }else{
                echo "Error en la conexion a la Db -> ".$this->conexion->connect_error();
            }
            $this->cerrar_conexion();
        }
    }
?>