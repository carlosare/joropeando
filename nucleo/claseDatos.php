<?php
	require_once('../../nucleo/claseDB.php');

	class Datos extends mySql{

		// Variables Usuarios

		public $_usuario;
		public $_pass;

		// Variables Grupos

		public $_idGrrupo;
		public $_nombreGrupo;
		public $_modalida;
		public $_categoria;
		public $_nombreAcademia;
		public $_nombreDirector;
		public $_apellidosDirector;
		public $_descripcion;
		public $_ubicacion;

		// Variables Jurados
		public $_idJurado;
		public $_jurado;

		// Variables calificaciones

		public $_ritmo;
		public $_dificulta;
		public $_zapateo;
		public $_creatividad;
		public $_dezplazamiento;
		public $_vestuario;
		public $_puntaje;

		// Validacion

		public $_idValidacion;
		public $_validador;

		// Funciones Usuarios



		public function set_usuario($usuario){
			$this->_usuario = $usuario;
		}

		public function get_usuario(){
			return $this->_usuario;
		}

		public function set_pass($pass){
			$this->_pass = $pass;
		}

		public function get_pass(){
			return $this->_pass;
		}

		// Funciones Grupos

		public function set_idGrupo($idGrupo){
			$this->_idGrupo = $idGrupo;
		}

		public function get_idGgrupo(){
			return $this->_idGrupo;
		}

		public function set_nombreGrupo($nombreGrupo){
			$this->_nombreGrupo = $nombreGrupo;
		}

		public function get_nombreGrupo(){
			return $this->_nombreGrupo;
		}

		public function set_modalida($modalida){
			$this->_modalida = $modalida;
		}

		public function get_modalida(){
			return $this->_modalida;
		}

		public function set_categoria($categoria){
			$this->_categoria = $categoria;
		}

		public function get_categoria(){
			return $this->_categoria;
		}

		public function set_nombreAcademia($nombreAcademia){
			$this->_nombreAcademia = $nombreAcademia;
		}

		public function get_nombreAcademia(){
			return $this->_nombreAcademia;
		}

		public function set_nombreDirector($nombreDirector){
			$this->_nombreDirector = $nombreDirector;
		}

		public function get_nombreDirector(){
			return $this->_nombreDirector;
		}

		public function set_apellidosDirector($apellidosDirector){
			$this->_apellidosDirector = $apellidosDirector;
		}

		public function get_apellidosDirector(){
			return $this->_apellidosDirector;
		}

		public function set_descripcion($descripcion){
			$this->_descripcion = $descripcion;
		}

		public function get_descripcion(){
			return $this->_descripcion;
		}

		public function set_ubicacion($ubicacion){
			$this->_ubicacion = $ubicacion;
		}

		public function get_ubicacion(){
			return $this->_ubicacion;
		}

		// Funciones Jurados

		public function set_idJurado($idJurado){
			$this->_idJurado = $idJurado;
		}

		public function set_jurado($jurado){
			$this->_jurado = $jurado;
		}

		// Funciones Calificaciones

		public function set_ritmo($ritmo){
			$this->_ritmo = $ritmo;
		}

		public function set_dificulta($dificulta){
			$this->_dificulta = $dificulta;
		}

		public function set_zapateo($zapateo){
			$this->_zapateo = $zapateo;
		}

		public function set_creatividad($creatividad){
			$this->_creatividad = $creatividad;
		}

		public function set_dezplazamiento($dezplazamiento){
			$this->_dezplazamiento = $dezplazamiento;
		}

		public function set_vestuario($vestuario){
			$this->_vestuario = $vestuario;
		}

		public function set_puntaje($puntaje){
			$this->_puntaje = $puntaje;
		}
		
		// Funciones de Validacion

		public function set_idValidacion($idValidacion){
			$this->_idValidacion = $idValidacion;
		}

		public function get_idValidacion(){
			return $this->_idValidacion;
		}

		public function set_validador($validador){
			$this->_validador = $validador;
		}

		public function get_validador(){
			return $this->_validador;
		}
	}